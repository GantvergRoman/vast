const connection = require('../utils/connection');
const xml = require('./xmlcontroller');

//TODO: decide build connection every query or build it one time or ???

module.exports = {
    insert: function (data, callback, error) {
        const dbQuery = 'INSERT INTO vast(vast_url, position, hide_ui) VALUES (?, ?, ?)';
        const params = [data.vastURL, data.position, data.hideUI ? 1 : 0];
        connection.query(dbQuery, params, (err, fields) => {
            err === null ? callback(fields.insertId) : error(500);

        })
    },
    select: function (id, callback, error) {
        const dbQuery = 'SELECT * FROM vast WHERE id = ?';
        const params = [id];
        connection.query(dbQuery, params, (err, fields) => {
            let data;
            if (fields.length === 0) {
                data = undefined
            } else {
                data = {
                    vastURL: fields[0].vast_url,
                    position: fields[0].position,
                    hideUI: fields[0].hide_ui === 1,
                    videoId: fields[0].id
                }
            }
            err === null ? callback(xml.build(data)) : error(500)
        });
    }
}