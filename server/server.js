const
    http = require('http'),
    express = require('express'),
    app = express(),
    headers = require('../utils/headers'),
    router = require('./router'),
    httpServer = http.createServer(app),

    PORT = 8081;

app.use('*', function (req, res, next) {
    headers.setHeaders(res);
    next();
});

httpServer.listen(PORT);
console.log('Server is listening on '+PORT);

app.use('/', router);
