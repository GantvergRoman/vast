const xml = require('xmlbuilder');
const
    VERSION = '2.0',
    AD_ID = 'ComboGuard',
    DURATION = '00:00:19',
    FLASH = {
        type:'application/x-shockwave-flash',
        ext: 'swf'
    },
    JS = {
        type: "application/javascript",
        ext: 'js'
    },
    API_FRAMEWORK = 'VPAID',
    HEIGHT = '168',
    WIDTH = '298',
    DELIVERY = 'progressive',
    URL = 'http://localhost/ComboWrapper.';

module.exports = {
    //TODO: Build using loops
    build: function(data) {
        const doc = xml.create('VAST');
        doc.att('version', VERSION);
        if (data !== undefined) {
            doc.ele('Ad')
                .att('id', AD_ID)
                .ele('InLine')
                    .ele('AdSystem')
                        .txt(VERSION)
                        .up()
                    .ele('Impression')
                        .up()
                    .ele('Creatives')
                        .ele('Creative')
                            .ele('Linear')
                                .ele('Duration')
                                    .txt(DURATION) //TODO: Where find duration?
                                    .up()
                                .ele('MediaFiles')
                                    .ele('MediaFile')
                                        .att('type', FLASH.type)
                                        .att('apiFramework', API_FRAMEWORK)
                                        .att('height', HEIGHT)
                                        .att('width', WIDTH)
                                        .att('delivery', DELIVERY)
                                        .dat(buildUrl(data, FLASH.ext))
                                        .up()
                                    .ele('MediaFile')
                                        .att('type', JS.type)
                                        .att('apiFramework', API_FRAMEWORK)
                                        .att('height', HEIGHT)
                                        .att('width', WIDTH)
                                        .att('delivery', DELIVERY)
                                        .dat(buildUrl(data, JS.ext))
                                        .up()
                                    .up()
                                .up()
                            .up()
                        .up()
                    .up()
                .up();
        };
        doc.end();
        const result = doc.toString();
        console.log(result);
        return result;
    }

}

function buildUrl(data, ext) {
    const params = ['vast='+data.vastURL, 'position='+data.position, 'hideUI='+data.hideUI, 'videoId='+data.videoId];
    const result = 'http://localhost/ComboWrapper.'+ext+'?'+params.join('&');
    return result;
}