const
    express = require('express'),
    router = express.Router(),
    db = require('./dbcontroller');

router.post('/create_vast', (req, res) => {
    var data = {
        vastURL: req.query.vastURL,
        position: req.query.position,
        hideUI: req.query.hideUI === 'true',
    }

    validate(data,
        () => { //ok
            db.insert(data,
                () => {
                    res.status(200);
                    res.end();
                },
                (err) => {
                    res.status(500).json(err);
                    res.end();
                })
        },
        (err) => {
            res.status(400).json(err);
            res.end();
        }
    );
});

router.get('/fetch_vast', (req, res) => {
    var id = req.query.id;

    db.select(id,
        (response) => { //ok
            res.status(200).send(response); //TODO: has to send XML
            res.end();

        },
        (err) => {
            res.status(400).json(err);
            res.end();
        })
});

function validate(data, callback, error) {
    // TODO: realize validation data
    callback(true);
}

module.exports = router;